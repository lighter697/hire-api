<?php

namespace App\Transformer;

use App\Entity\Basic;
use League\Fractal\TransformerAbstract;

class BasicTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'picture',
        'profiles',
        'work',
        'educations',
        'skills',
        'languages',
        'references',
        'currentJob',
        'lastEducation',
    ];

    protected $defaultIncludes = [
        'picture',
        'currentJob',
        'lastEducation',
    ];

    /**
     * @param Basic $basic
     * @return array
     * @throws \Exception
     */
    public function transform(Basic $basic)
    {
        return [
            'id' => $basic->getId(),
            'firstName' => $basic->getFirstName(),
            'lastName' => $basic->getLastName(),
            'label' => $basic->getLabel(),
            'email' => $basic->getEmail(),
            'phone' => $basic->getPhone(),
            'skype' => $basic->getSkype(),
            'summary' => $basic->getSummary(),
            'address' => $basic->getAddress(),
            'experience' => $basic->getExperience(),
            'experienceLastJob' => $basic->getExperienceLastJob(),
            'age' => $basic->getAge(),
            'firstJob' => $basic->getFirstJob() ? (!empty($basic->getFirstJob()->getStartDate()) ? $basic->getFirstJob()->getStartDate()->format('Y-m-d') : null) : null,
            'lastJob' => $basic->getCurrentJob() ? (!empty($basic->getCurrentJob()->getStartDate()) ? $basic->getCurrentJob()->getStartDate()->format('Y-m-d') : null) : null,
            'birthDate' => $basic->getBirthDate() ? $basic->getBirthDate()->format('Y-m-d') : null,
        ];
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Item
     */
    public function includePicture(Basic $basic)
    {
        if ($basic->getPicture()) {
            return $this->item($basic->getPicture(), new PictureTransformer);
        }
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProfiles(Basic $basic)
    {
        return $this->collection($basic->getProfiles(), new ProfileTransformer);
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeWork(Basic $basic)
    {
        return $this->collection($basic->getWork(), new WorkTransformer());
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeEducations(Basic $basic)
    {
        return $this->collection($basic->getEducations(), new EducationTransformer());
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSkills(Basic $basic)
    {
        return $this->collection($basic->getSkills(), new SkillTransformer());
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeLanguages(Basic $basic)
    {
        return $this->collection($basic->getLanguages(), new LanguageTransformer());
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Collection
     */
    public function includeReferences(Basic $basic)
    {
        return $this->collection($basic->getReferences(), new ReferenceTransformer());
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Item
     */
    public function includeCurrentJob(Basic $basic)
    {
        if ($basic->getCurrentJob()) {
            return $this->item($basic->getCurrentJob(), new WorkTransformer());
        }
    }

    /**
     * @param Basic $basic
     * @return \League\Fractal\Resource\Item
     */
    public function includeLastEducation(Basic $basic)
    {
        if ($basic->getLastEducation()) {
            return $this->item($basic->getLastEducation(), new EducationTransformer());
        }
    }
}
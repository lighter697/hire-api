<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Profile;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
    public function transform(Profile $profile)
    {
        return [
            'id' => $profile->getId(),
            'url' => $profile->getUrl(),
            'network' => $profile->getNetwork(),
            'username' => $profile->getUsername(),
        ];
    }
}
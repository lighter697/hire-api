<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Education;
use App\Entity\Language;
use App\Entity\Profile;
use App\Entity\Reference;
use App\Entity\Skill;
use App\Entity\Work;
use League\Fractal\TransformerAbstract;

class ReferenceTransformer extends TransformerAbstract
{
    public function transform(Reference $reference)
    {
        return [
            'id' => $reference->getId(),
            'name' => $reference->getName(),
            'reference' => $reference->getReference(),
        ];
    }
}
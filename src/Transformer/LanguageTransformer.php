<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Education;
use App\Entity\Language;
use App\Entity\Profile;
use App\Entity\Skill;
use App\Entity\Work;
use League\Fractal\TransformerAbstract;

class LanguageTransformer extends TransformerAbstract
{
    public function transform(Language $language)
    {
        return [
            'id' => $language->getId(),
            'language' => $language->getLanguage(),
            'fluency' => $language->getFluency(),
        ];
    }
}
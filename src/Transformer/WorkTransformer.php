<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Profile;
use App\Entity\Work;
use League\Fractal\TransformerAbstract;

class WorkTransformer extends TransformerAbstract
{
    public function transform(Work $work)
    {
        return [
            'id' => $work->getId(),
            'url' => $work->getCompany(),
            'position' => $work->getPosition(),
            'company' => $work->getCompany(),
            'startDate' => (!empty($work->getStartDate()) ? $work->getStartDate()->format('Y-m-d') : null),
            'endDate' => (!empty($work->getEndDate()) ? $work->getEndDate()->format('Y-m-d') : null),
            'summary' => $work->getSummary(),
            'website' => $work->getWebsite(),
        ];
    }
}
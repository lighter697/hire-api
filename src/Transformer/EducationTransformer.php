<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Education;
use App\Entity\Profile;
use App\Entity\Work;
use League\Fractal\TransformerAbstract;

class EducationTransformer extends TransformerAbstract
{
    public function transform(Education $education)
    {
        return [
            'id' => $education->getId(),
            'school' => $education->getSchool(),
            'fieldOfStudy' => $education->getFieldOfStudy(),
            'degree' => $education->getDegree(),
            'startDate' => (!empty($education->getStartDate()) ? $education->getStartDate()->format('Y-m-d') : null),
            'endDate' => (!empty($education->getEndDate()) ? $education->getEndDate()->format('Y-m-d') : null),
            'description' => $education->getDescription(),
        ];
    }
}
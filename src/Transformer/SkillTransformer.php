<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Education;
use App\Entity\Profile;
use App\Entity\Skill;
use App\Entity\Work;
use League\Fractal\TransformerAbstract;

class SkillTransformer extends TransformerAbstract
{
    public function transform(Skill $skill)
    {
        return [
            'id' => $skill->getId(),
            'name' => $skill->getName(),
            'level' => $skill->getLevel(),
        ];
    }
}
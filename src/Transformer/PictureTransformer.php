<?php

namespace App\Transformer;

use App\Entity\Basic;
use App\Entity\Image;
use League\Fractal\TransformerAbstract;

class PictureTransformer extends TransformerAbstract
{
    /**
     * @param Image $image
     * @return array
     */
    public function transform(Image $image)
    {
        return [
            'id' => $image->getId(),
            'filename' => $image->getImageName(),
        ];
    }
}
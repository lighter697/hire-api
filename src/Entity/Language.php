<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    const ELEMENTARY = 'ELEMENTARY';
    const LIMITED_WORKING = 'LIMITED_WORKING';
    const PROFESSIONAL_WORKING = 'PROFESSIONAL_WORKING';
    const FULL_PROFESSIONAL_WORKING = 'FULL_PROFESSIONAL_WORKING';
    const NATIVE_OR_BILINGUAL = 'NATIVE_OR_BILINGUAL';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fluency;

    /**
     * @var Basic
     * @ORM\ManyToOne(targetEntity="Basic", inversedBy="languages")
     */
    private $basic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getFluency(): ?string
    {
        return $this->fluency;
    }

    public function setFluency(string $fluency): self
    {
        $this->fluency = $fluency;

        return $this;
    }

    /**
     * @return Basic
     */
    public function getBasic(): Basic
    {
        return $this->basic;
    }

    /**
     * @param Basic $basic
     * @return Language
     */
    public function setBasic($basic): Language
    {
        $this->basic = $basic;
        return $this;
    }


}

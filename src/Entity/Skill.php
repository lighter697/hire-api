<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkillRepository")
 */
class Skill
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $level;

    /**
     * @var Basic
     * @ORM\ManyToOne(targetEntity="Basic", inversedBy="skills")
     */
    private $basic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Basic
     */
    public function getBasic(): Basic
    {
        return $this->basic;
    }

    /**
     * @param Basic $basic
     * @return Skill
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return Skill
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }
}

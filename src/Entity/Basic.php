<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BasicRepository")
 */
class Basic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $skype;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @var Location
     * @ORM\Embedded(class="App\Entity\Location")
     */
    private $location;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Profile", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    public $profiles;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Work", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    private $work;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Education", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    private $educations;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Skill", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    private $skills;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Language", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    private $languages;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Reference", mappedBy="basic", cascade={"persist"}, orphanRemoval=true)
     */
    private $references;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
        $this->work = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->references = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Image
     */
    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * @param Profile $profile
     * @return Basic
     */
    public function addProfile($profile)
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles->add($profile);
            $profile->setBasic($this);
        }
        return $this;
    }

    /**
     * @param $profile Profile
     * @return $this
     */
    public function removeProfile($profile)
    {
        if ($this->profiles->contains($profile)) {
            $this->profiles->removeElement($profile);
            $profile->setBasic(null);
        }
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * @param Work $work
     * @return Basic
     */
    public function addWork($work)
    {
        if (!$this->work->contains($work)) {
            $this->work->add($work);
            $work->setBasic($this);
        }
        return $this;
    }

    /**
     * @param Work $work
     * @return $this
     */
    public function removeWork($work)
    {
        if ($this->work->contains($work)) {
            $this->work->removeElement($work);
            $work->setBasic(null);
        }
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * @param Education $education
     * @return Basic
     */
    public function addEducation($education)
    {
        if (!$this->educations->contains($education)) {
            $this->educations->add($education);
            $education->setBasic($this);
        }
        return $this;
    }

    /**
     * @param $education Education
     * @return $this
     */
    public function removeEducation($education)
    {
        if ($this->educations->contains($education)) {
            $this->educations->removeElement($education);
            $education->setBasic(null);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param Skill $skill
     * @return Basic
     */
    public function addSkill($skill)
    {
        if (!$this->skills->contains($skill)) {
            $this->skills->add($skill);
            $skill->setBasic($this);
        }
        return $this;
    }

    /**
     * @param $skill Skill
     * @return $this
     */
    public function removeSkill($skill)
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
            $skill->setBasic(null);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param Language $language
     * @return Basic
     */
    public function addLanguage($language)
    {
        if (!$this->languages->contains($language)) {
            $this->languages->add($language);
            $language->setBasic($this);
        }
        return $this;
    }

    /**
     * @param $language Language
     * @return $this
     */
    public function removeLanguage($language)
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
            $language->setBasic(null);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Basic
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return Basic
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * @param $reference Reference
     * @return $this
     */
    public function addReference($reference)
    {
        if (!$this->references->contains($reference)) {
            $this->references->add($reference);
            $reference->setBasic($this);
        }
        return $this;
    }

    /**
     * @param $reference Reference
     * @return $this
     */
    public function removeReference($reference)
    {
        if ($this->references->contains($reference)) {
            $this->references->removeElement($reference);
            $reference->setBasic(null);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param mixed $skype
     * @return Basic
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Basic
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     * @return Basic
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    public function getExperience()
    {
        if ($this->getFirstJob()) {
            $startDate = $this->getFirstJob()->getStartDate();
            if ($startDate) {
                return $startDate->diff(new \DateTime('now'))->format('%y');
            }
        }

        return null;
    }

    public function getExperienceLastJob()
    {
        if ($this->getCurrentJob()) {
            $startDate = $this->getCurrentJob()->getStartDate();
            if ($startDate) {
                return $startDate->diff(new \DateTime('now'))->format('%y');
            }
        }

        return null;
    }
    /**
     * @return Work | null
     */
    public function getFirstJob()
    {
        $iterator = $this->getWork()->getIterator();
        $iterator->uasort(function ($a, $b) {
            /**
             * @var $a Work
             * @var $b Work
             */
            return ($a->getStartDate() < $b->getStartDate()) ? -1 : 1;
        });
        $array = iterator_to_array($iterator);
        return array_shift($array);
    }

    /**
     * @return Work | null
     */
    public function getCurrentJob()
    {
        $iterator = $this->getWork()->getIterator();
        $iterator->uasort(function ($a, $b) {
            /**
             * @var $a Work
             * @var $b Work
             */
            return ($a->getStartDate() > $b->getStartDate()) ? -1 : 1;
        });
        $array = iterator_to_array($iterator);
        return array_shift($array);
    }

    public function getLastEducation()
    {
        $iterator = $this->getEducations()->getIterator();
        $iterator->uasort(function ($a, $b) {
            /**
             * @var $a Work
             * @var $b Work
             */
            return ($a->getStartDate() > $b->getStartDate()) ? -1 : 1;
        });
        $array = iterator_to_array($iterator);
        return array_shift($array);
    }

    /**
     * @return Education | null
     */
    public function getFirstEducation()
    {
        $iterator = $this->getEducations()->getIterator();
        $iterator->uasort(function ($a, $b) {
            /**
             * @var $a Work
             * @var $b Work
             */
            return ($a->getStartDate() < $b->getStartDate()) ? -1 : 1;
        });
        $array = iterator_to_array($iterator);
        return array_shift($array);
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getBirthDate()
    {
        if ($this->getFirstEducation()) {
            if ($this->getFirstEducation()->getStartDate()) {
                return (clone $this->getFirstEducation()->getStartDate())->sub(new \DateInterval('P18Y'));
            }
        }

        return null;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getAge()
    {
        if ($this->getBirthDate()) {
            return (clone $this->getBirthDate()->diff(new \DateTime()))->format('%y');
        }

        return null;
    }


}

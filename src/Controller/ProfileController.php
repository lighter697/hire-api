<?php

namespace App\Controller;

use App\Entity\Basic;
use App\Entity\Location;
use App\Form\BasicType;
use App\Serializer\DataSerializer;
use App\Transformer\BasicTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchPhrase;
use Elastica\Query\QueryString;
use Elastica\Query\Range;
use Elastica\Query\Term;
use FOS\ElasticaBundle\Finder\FinderInterface;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use GuzzleHttp\Client;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends RestController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var FinderInterface
     */
    private $finder;
    /**
     * @var Client
     */
    private $client;

    public function __construct(EntityManagerInterface $entityManager, PaginatedFinderInterface $finder)
    {
        $this->em = $entityManager;
        $this->finder = $finder;
        $this->client = new Client();
    }

    /**
     * @Route("/profile", methods={"GET"}, name="index")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $boolRoot = new BoolQuery();

        if ($request->get('keyword')) {

            $querySting = new QueryString();
            $querySting->setQuery($request->get('keyword'));

            $bool = new BoolQuery();
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        if ($request->get('company')) {

            $bool = new BoolQuery();

            $querySting = new QueryString();
            if ($request->get('currentCompany')) {
                $querySting->setDefaultField('currentJob.company');
            } else {
                $querySting->setDefaultField('work.company');
            }
            $querySting->setQuery($request->get('company'));
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        if ($request->get('position')) {
            $bool = new BoolQuery();

            $querySting = new QueryString();
            if ($request->get('currentPosition')) {
                $querySting->setDefaultField('currentJob.position');
            } else {
                $querySting->setDefaultField('work.position');
            }
            $querySting->setQuery($request->get('position'));
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        if ($request->get('skill')) {

            $querySting = new QueryString();
            $querySting->setDefaultField('skills.name');
            $querySting->setQuery($request->get('skill'));

            $bool = new BoolQuery();
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        if ($request->get('city')) {
            $location = $this->findLocation($request->get('city'));
            if ($location) {
                $bool = new BoolQuery();
                if ($location->getCountry()) {
                    $bool->addMust(new MatchPhrase('location.country', $location->getCountry()));
                }
                if ($location->getRegion()) {
                    $bool->addMust(new MatchPhrase('location.region', $location->getRegion()));
                }
                if ($location->getCity()) {
                    $bool->addMust(new MatchPhrase('location.city', $location->getCity()));
                }
                $boolRoot->addMust($bool);
            }

        }

        if ($request->get('education')) {

            $querySting = new QueryString();
            $querySting->setDefaultField('educations.school');
            $querySting->setQuery($request->get('education'));

            $bool = new BoolQuery();
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        if ($request->get('language')) {

            $querySting = new QueryString();
            $querySting->setDefaultField('languages.language');
            $querySting->setQuery($request->get('language'));

            $bool = new BoolQuery();
            $bool->addMust($querySting);

            $boolRoot->addMust($bool);
        }

        // Filters
        if ($request->get('opened')) {

            $bool = new BoolQuery();

            $term = new Term();
            $term->setTerm('label', 'looking');
            $bool->addShould($term);

            $term = new Term();
            $term->setTerm('label', 'position');
            $bool->addShould($term);

            $term = new Term();
            $term->setTerm('label', 'open');
            $bool->addShould($term);

            $term = new Term();
            $term->setTerm('label', 'offers');
            $bool->addShould($term);

            $term = new Term();
            $term->setTerm('label', 'job');
            $bool->addShould($term);

            $term = new Term();
            $term->setTerm('label', 'opportunities');
            $bool->addShould($term);

            $boolRoot->addMust($bool);
        }

        if ($request->get('minExperience')) {
            $condition = new Range('experience', [
                'gte' => $request->get('minExperience')
            ]);
            $boolRoot->addMust($condition);
        }

        if ($request->get('maxExperience')) {
            $condition = new Range('experience', [
                'lte' => $request->get('maxExperience')
            ]);
            $boolRoot->addMust($condition);
        }

        if ($request->get('minCurrentJob')) {
            $condition = new Range('experienceLastJob', [
                'gte' => $request->get('minCurrentJob')
            ]);
            $boolRoot->addMust($condition);
        }

        if ($request->get('maxCurrentJob')) {
            $condition = new Range('experienceLastJob', [
                'lte' => $request->get('maxCurrentJob')
            ]);
            $boolRoot->addMust($condition);
        }

        if ($request->get('minAge')) {
            $condition = new Range('age', [
                'gte' => $request->get('minAge')
            ]);
            $boolRoot->addMust($condition);
        }

        if ($request->get('maxAge')) {
            $condition = new Range('age', [
                'lte' => $request->get('maxAge')
            ]);
            $boolRoot->addMust($condition);
        }

        $finalQuery = new Query($boolRoot);
        $finalQuery->setSort(array('id' => array('order' => 'desc')));

        $paginator = $this->finder->findPaginated($finalQuery);
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($request->get('page', 1));
        $fractal = new Manager();
        $fractal->setSerializer(new DataSerializer());
        $fractal->parseIncludes([
            'picture',
            'profiles',
            'work',
            'educations',
            'skills',
            'languages',
            'references',
        ]);
        $resource = new Collection($paginator->getIterator(), new BasicTransformer());
        return new JsonResponse([
            'total' => $paginator->getNbResults(),
            'data' => $fractal->createData($resource)->toArray()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/profile", methods={"POST"}, name="create_profile")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $entity = new Basic();
        $form = $this->createForm(BasicType::class, $entity);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            if ($entity->getAddress()) {
                $entity->setLocation($this->findLocation($entity->getAddress()));
            }
            $this->em->persist($entity);
            $this->em->flush();
            return new JsonResponse(['success']);
        }

        return new JsonResponse($this->getFormErrorsAsArray($form));
    }

    /**
     * @param $address
     *
     * @return Location|null
     */
    private function findLocation($address)
    {
        // Get location from google
        $data = json_decode($this->client->get('https://maps.googleapis.com/maps/api/geocode/json?key=' . getenv('GOOGLE_API_KEY') . '&language=en&address=' . $address)->getBody()->getContents(), true);

        if (!empty($data['results'])) {
            if (!empty($data['results'][0]['address_components'])) {
                $location = new Location();
                foreach ($data['results'][0]['address_components'] as $item) {
                    if (in_array('locality', $item['types'])) {
                        $location->setCity($item['long_name']);
                    }
                    if (in_array('administrative_area_level_1', $item['types'])) {
                        $location->setRegion($item['long_name']);
                    }
                    if (in_array('country', $item['types'])) {
                        $location->setCountry($item['long_name']);
                    }
                }
                if (!empty($data['results'][0]['geometry'])) {
                    $location->setLat($data['results'][0]['geometry']['location']['lat']);
                    $location->setLng($data['results'][0]['geometry']['location']['lng']);
                }

                return $location;
            }
        }

        return null;
    }

    /**
     * @Route("/profile/find", methods={"POST"}, name="find_profile")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findAction(Request $request)
    {
        $fractal = new Manager();
        $fractal->setSerializer(new DataSerializer());
        $repository = $this->em->getRepository(Basic::class);
        $user = null;
        // Find by email
        if ($request->get('email')) {
            $user = $repository->findOneBy(['email' => $request->get('email')]);
        }

        // Find by name
        if (empty($user) && $request->get('firstName') && $request->get('lastName')) {
            $user = $repository->findOneBy(['firstName' => $request->get('firstName'), 'lastName' => $request->get('lastName')]);
        }

        if (empty($user)) {
            return new JsonResponse(null, Response::HTTP_OK);
        }

        return new JsonResponse([
            'id' => $user->getId(),
            'avatar' => (!empty($user->getPicture()) ? $user->getPicture()->getId() : null)
        ], Response::HTTP_OK);

    }

    /**
     * @Route("/profile/{id}", methods={"POST"}, name="update_profile")
     * @param Basic   $basic
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateAction(Basic $basic, Request $request)
    {
        $form = $this->createForm(BasicType::class, $basic);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $basic->setLocation($this->findLocation($basic->getAddress()));
            $this->em->persist($basic);
            $this->em->flush();
            return new JsonResponse(['success']);
        }

        return new JsonResponse($this->getFormErrorsAsArray($form));
    }

    /**
     * @Route("/profile/{id}", methods={"GET"}, name="show_profile")
     * @param Basic $basic
     *
     * @return JsonResponse
     */
    public function showAction(Basic $basic)
    {
        $fractal = new Manager();
        $fractal->setSerializer(new DataSerializer());
        $fractal->parseIncludes([
            'picture',
            'profiles',
            'work',
            'educations',
            'skills',
            'languages',
            'references',
        ]);
        return new JsonResponse($fractal->createData(new Item($basic, new BasicTransformer()))->toArray(), Response::HTTP_OK);
    }
}

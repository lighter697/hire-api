<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;


class ImageController extends RestController
{
    private $em;
    private $kernel;

    public function __construct(EntityManagerInterface $entityManager, KernelInterface $kernel)
    {
        $this->em = $entityManager;
        $this->kernel = $kernel;
    }

    /**
     * @REST\Post("/image")
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $image = new Image();
        $form = $this->createForm(ImageType::class, $image);
        $form->submit($request->files->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($image);
            $this->em->flush();

            return new JsonResponse([
                'id' => $image->getId(),
                'url' => getenv('BASE_URL') . '/images/avatars/' . $image->getImageName(),
            ]);
        }

        return new JsonResponse($this->getFormErrorsAsArray($form), Response::HTTP_BAD_REQUEST);
    }

    public function getFormErrorsAsArray(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrorsAsArray($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}

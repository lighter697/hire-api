<?php

namespace App\Form;

use App\Entity\Basic;
use App\Entity\Location;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class BasicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('lastName', TextType::class, array(
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('label', TextType::class)
            ->add('picture')
            ->add('email', EmailType::class)
            ->add('phone', TextType::class)
            ->add('skype', TextType::class)
            ->add('summary', TextType::class)
            ->add('address', TextType::class)
            ->add('profiles', CollectionType::class, array(
                'entry_type' => ProfileType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('work', CollectionType::class, array(
                'entry_type' => WorkType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('educations', CollectionType::class, array(
                'entry_type' => EducationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('skills', CollectionType::class, array(
                'entry_type' => SkillType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('languages', CollectionType::class, array(
                'entry_type' => LanguageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('references', CollectionType::class, array(
                'entry_type' => ReferenceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Basic::class,
            'csrf_protection' => false,
            'method' => 'POST',
            'error_bubbling' => false
        ]);
    }
}

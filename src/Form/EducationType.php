<?php

namespace App\Form;

use App\Entity\Education;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('school', TextType::class, array(
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->add('degree', TextType::class)
            ->add('fieldOfStudy', TextType::class)
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('description', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
        ]);
    }
}

<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StackoverflowSynonymsCommand extends Command
{
    protected static $defaultName = 'app:stackoverflow-synonyms';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tagsFile = file_get_contents(__DIR__ . '/tags.csv');
        $synonymsFile = file_get_contents(__DIR__ . '/synonyms.csv');
        foreach (explode(PHP_EOL, $tagsFile) as $key => $row) {
            $cols = explode(",", $row);
            $tag = substr($cols[1], 1, -1);
            $synonyms = [$tag];
            foreach (explode(PHP_EOL, $synonymsFile) as $r) {

                $c = explode(",", $r);
                $source = substr($c[1], 1, -1);
                $target = substr($c[2], 1, -1);

                if (trim($source) === trim($tag)) {
                    $synonyms[] = $target;
                }

                if (trim($target) === trim($tag)) {
                    $synonyms[] = $source;
                }
            }
            $output->writeln($key . ' - ' . implode(',', array_unique($synonyms)));
            $resultFile = file_get_contents(__DIR__ . '/result.csv');
            $resultFile = $resultFile . implode(',', array_unique($synonyms)) . PHP_EOL;
            file_put_contents(__DIR__ . '/result.csv', $resultFile);
        }
    }
}

<?php

namespace App\Command;

use App\Entity\Basic;
use App\Entity\Education;
use App\Entity\Language;
use App\Entity\Location;
use App\Entity\Profile;
use App\Entity\Skill;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Date;

class GenerateDummyProfilesCommand extends Command
{
    protected static $defaultName = 'GenerateDummyProfiles';
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->em = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $basic = new Basic();
        $basic->setFirstName('Tom');
        $basic->setLastName('Francis');
        $basic->setLabel('Senior Contract Recruitment Consultant');
        $basic->setPicture('https://media.licdn.com/dms/image/C5603AQGjzxFRPTtGzQ/profile-displayphoto-shrink_800_800/0?e=1551312000&v=beta&t=CDOhFYpAelkJpwIG_nhm-7jdrA-s8xc4TH-JnK0GYZM');
        $basic->setEmail('tom@thomasfrancis.co.uk');
//        $basic->setPhone();
//        $basic->setWebsite($faker->url);
        $basic->setSummary('Pearson Frank is a leading Java, Web and Mobile Development recruitment organization with global coverage. We aim to be widely recognised as the most trusted Java, Web and Mobile Development recruitment firm worldwide. 

Our mission is to connect clients with top Java, Web and Mobile Development opportunities. By focusing solely on Java, Web and Mobile Development, our consultants are genuine experts, meaning they not only fully understand the market, but have built solid, exclusive relationships with the widest range of vendors, customers and specialists looking to progress their career.
We always have live positions for:

-	JAVA Jobs
-	PHP Jobs
- UI/UX Jobs
-	Android Jobs
-	IOS Jobs
-	AngularJS Jobs
-	Python Jobs
-	Ruby Jobs

Our team at Pearson Frank are a part of Frank Recruitment Group, a pioneering niche IT recruitment business recently backed by private equity firm TPG Growth, which has invested in some of the most innovative and transformative companies in the world.

If you are a Java, Web and Mobile Development professional looking for a cutting-edge, exciting opportunity, or a company that would like to learn more about how Pearson Frank can add value to your business and projects, then please feel free to contact me t.francis@pearsonfrank.com, 0191 338 7543. I look forward to connecting with you!

Check out pearsonfrank.com, follow our LinkedIn page and connect with me on LinkedIn for the latest Java, Web and Mobile Development updates.');
        $basic->setIndustry('Senior Contract Recruitment Consultant');

        $location = new Location();
        $location->setCountryCode('gb');
        $location->setAddress('Newcastle upon Tyne, United Kingdom');
//        $location->setCity();
        $location->setPostalCode('NE66');
//        $location->setRegion($faker->streetAddress);
        $basic->setLocation($location);

        $education = new Education();
        $education->setArea('3D Design');
        $education->setInstitution('Newcastle College');
        $education->setStudyType('Extended Diploma');
        $education->setStartDate((new \DateTime())->setDate(2011, 1, 1));
        $education->setEndDate((new \DateTime())->setDate(2014, 1, 1));
//        $education->setGpa(4.0);
        $basic->addEducation($education);

        $education = new Education();
//        $education->setArea('3D Design');
        $education->setInstitution('Duchess Community High School');
        $education->setStudyType('GCSE');
        $education->setStartDate((new \DateTime())->setDate(2007, 1, 1));
        $education->setEndDate((new \DateTime())->setDate(2011, 1, 1));
//        $education->setGpa(4.0);
        $basic->addEducation($education);

        $language = new Language();
        $language->setLanguage('en');
        $language->setFluency('fluent');
        $basic->addLanguage($language);

        $profile = new Profile();
        $profile->setUrl('https://www.linkedin.com/in/tom-francis-863a6513b/');
        $profile->setUsername('tom-francis-863a6513b');
        $profile->setNetwork('linkedin');
        $basic->addProfile($profile);

        $skill = new Skill();
        $skill->setName('Recruiting');
        $skill->setLevel('master');
        $basic->addSkill($skill);

        $skill = new Skill();
        $skill->setName('Sales');
        $skill->setLevel('master');
        $basic->addSkill($skill);

        $skill = new Skill();
        $skill->setName('Marketing');
        $skill->setLevel('master');
        $basic->addSkill($skill);

        $this->em->persist($basic);
        $this->em->flush();
    }
}

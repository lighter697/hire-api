* Create admin user
```bash 
$ sudo docker exec -it hire-api-php-fpm php bin/console fos:user:create --super-admin <username> <email> <password>
```

* Create oauth client
```bash 
$ sudo docker exec -it hire-api-php-fpm php bin/console fos:oauth-server:create-client --grant-type=password --grant-type=refresh_token --grant-type=client_credentials
```

* Execute SQL query (set correct client_id and client_secret)
```
UPDATE `oauth_clients` SET
`random_id` = '3yocqglezym804gcccko0gwwggs44g48gokgo4co4k8so4so80',
`secret` = '1eliyxd9zyck8gk84ck0cswggo00s088koc8ogc0kww4w848sw'
WHERE `id` = '1';
```

* Populate elastic search indexes
```bash
 sudo docker exec -it hire-api-php-fpm php bin/console fos:elastica:populate
```
